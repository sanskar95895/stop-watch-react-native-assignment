import { StyleSheet, Text, View, Button } from "react-native";

import { useState } from "react";

const Start = (props) => {
  function f() {
    props.setFlag(2);
  }
  return (
    <View>
      <Button
        disabled={props.flag === 2}
        onPress={f}
        title="Start"
        color="green"
      ></Button>
    </View>
  );
};


export default Start;
