import { StyleSheet, Text, View, Button } from "react-native";

import { useState } from "react";

const Pause = (props) => {
  return (
    <View>
      <Button
        disabled={props.flag === 1 || props.flag === 3}
        onPress={() => {
          props.setFlag(3);
        }}
        title="Pause"
        color="orange"
      ></Button>
    </View>
  );
};


export default Pause;
