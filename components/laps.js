import { StyleSheet, Text, View, Button } from "react-native";

import { useState } from "react";

const Lap = (props) => {
  function AddLaps() {
    props.setLaps([
      ...props.laps,
      [props.laps.length + 1, props.hour, props.min, props.sec, props.mili],
    ]);
  }
  return (
    <View>
      <Button onPress={AddLaps} title="Laps" color="violet"></Button>
    </View>
  );
};

export default Lap;
