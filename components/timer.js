import { StyleSheet, Text, View } from 'react-native';

import { Double } from "./test.js";
import { useState } from 'react';


const Timer = (props) => {
    return (  
        <View>
            <Text >{Double(props.hour)} :</Text>
            <Text >{Double(props.min)} :</Text>
            <Text >{Double(props.sec)} :</Text>
            <Text >{Double(props.mili)} </Text>

        </View>);
}


 
export default Timer;